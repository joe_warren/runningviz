window.onload = function(){
    var map = new L.Map("map", {center: [52.1936, -2.2216], zoom: 14})
        .addLayer(new L.TileLayer("http://{s}.tile.stamen.com/toner-lite/{z}/{x}/{y}.png"));

    var svg = d3.select(map.getPanes().overlayPane).append("svg"),
    g = svg.append("g").attr("class", "leaflet-zoom-hide");

    d3.xml("run.tcx", function(error, data) {
       if (error) throw error;
       window.console.log(data);
       var points = [].map.call(data.querySelectorAll("Trackpoint"), function(d){
           var lat = d.querySelector("LatitudeDegrees");
           var lon = d.querySelector("LongitudeDegrees");
           return {
             "distance": parseFloat(d.querySelector("DistanceMeters").textContent),
             "time": d3.isoParse(d.querySelector("Time").textContent),
             "lat": lat ? parseFloat(lat.textContent) : false,
             "lon": lon ? parseFloat(lon.textContent) : false
           }
       }).filter(d => d.lat && d.lon);
       map.panTo(new L.LatLng(
         d3.mean(points, d=> d.lat),
         d3.mean(points, d=> d.lon)
       ));

       points = points.filter(function(d,i){
         return i === 0 || d.distance != points[i-1].distance;
       });

       var lines = d3.pairs(points);

       var colour = d3.scaleLinear().domain([0.0,4.0])
          .interpolate(d3.interpolateHcl)
          .range([d3.rgb("#000055"), d3.rgb('#FF0000')]);

       var jog = g.selectAll("line").data(lines).enter()
            .append("line")
               .classed("jog", true)
            .attr("stroke", function(d){
                var timeElapsed = (d[1].time - d[0].time)/1000.0;
                var distance = d[1].distance - d[0].distance;
                var speed = distance/timeElapsed;
                return colour(speed);
            })
            .attr("style", function(d){
                return "animation-duration: " +  ((d[1].time - d[0].time)/1000.0) + "s;";
            });

      function reset() {
         function projectPoint(d) {
           return map.latLngToLayerPoint(new L.LatLng(d.lat, d.lon));
         } 

         jog.attr("x1", function(d){
             return projectPoint(d[0]).x;
           })
           .attr("y1", function(d){
             return projectPoint(d[0]).y;
           })
           .attr("x2", function(d){
             return projectPoint(d[1]).x;
           })
           .attr("y2", function(d){
             return projectPoint(d[1]).y;
           });

         var xVals = points.map(d=> projectPoint(d).x);
         var yVals = points.map(d=> projectPoint(d).y);
         var minX = Math.min.apply(null, xVals);
         var minY = Math.min.apply(null, yVals);
         var maxX = Math.max.apply(null, xVals);
         var maxY = Math.max.apply(null, yVals);
         svg .attr("width", maxX - minX +20 )
           .attr("height", maxY - minY + 20 )
           .style("left", (minX-10) + "px")
           .style("top", (minY-10) + "px");
  
        g.attr("transform", "translate(" + (-minX+10) + "," + (-minY+10) + ")");
          window.console.log("viewreset")
      };

      map.on("zoomend", reset);
      reset();
      window.console.log(points)

    });
};
